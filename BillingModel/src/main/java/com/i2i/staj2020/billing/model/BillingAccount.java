package com.i2i.staj2020.billing.model;

import java.sql.Timestamp;

/**
 * Billing account information holder
 *
 */
public class BillingAccount {

	private Long id;
	private Long billingAccountNo;
	private String name;
	private Long customerId;
	private String currency;
	private int billCycle;
	private String taxJurisdiction;
	private Timestamp lastBillDate;

	public BillingAccount() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getBillingAccountNo() {
		return billingAccountNo;
	}

	public void setBillingAccountNo(Long billingAccountNo) {
		this.billingAccountNo = billingAccountNo;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public int getBillCycle() {
		return billCycle;
	}

	public void setBillCycle(int billCycle) {
		this.billCycle = billCycle;
	}

	public String getTaxJurisdiction() {
		return taxJurisdiction;
	}

	public void setTaxJurisdiction(String taxJurisdiction) {
		this.taxJurisdiction = taxJurisdiction;
	}

	public Timestamp getLastBillDate() {
		return lastBillDate;
	}

	public void setLastBillDate(Timestamp lastBillDate) {
		this.lastBillDate = lastBillDate;
	}

}
