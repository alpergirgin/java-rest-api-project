/*
 * API CustomerBill
 *
 * ## TMF API Reference: TMF 678
 *  - Customer bill Management
 *
 * ### Release: 19.5
 *  The Customer Bill Management API allows to find and retrieve one or several customer bills (also called invoices) produced for a customer.
 *  A customer bill is an electronic or paper document produced at the end of the billing process.
 *  The customer bill gathers and displays different items (applied customer billing rates generated during the rating and billing processes) to be charged to a customer.
 *  It represents a total amount due for all the products during the billing period and all significant information like dates, bill reference...
 *  This API provides also operations to find and retrieve the details of applied customer billing rates presented on a customer bill.
 *  Finally, this API allows to request in real-time a customer bill creation and to manage this request.
 *
 * ### Resources
 *  - customerBill
 *  - appliedCustomerBillingRate
 *  - customerBillOnDemand
 *  - billingCycle
 *
 * ### Operations Customer Bill Management API performs the following operations :
 *  - Retrieve a customer bill or a collection of customer bills depending on filter criteria.
 *  - Partial update of  a customer bill (for administration purposes).
 *  - Retrieve an applied customer billing rate or a collection of applied customer billing rates depending on filter criteria.
 *  - Create a customer bill on demand request, retrieve one or a collection of  customer bill on demand request(s) depending on filter criteria.
 *  - Manage notification of events on customer bill and customer bill on demand.
 *
 *  Copyright © TM Forum 2018. All Rights Reserved.
 *
 * OpenAPI spec version: 4.0
 *
 */
package com.i2i.staj2020.billing.model.bill;

/**
 * The applied billing tax rate represents taxes applied billing rate it refers
 * to. It is calculated during the billing process.
 */
public class AppliedBillingTaxRate {
	private Long id = null;
	private String taxCategory = null;
	private String taxJurisdiction = null;
	private String taxType = null;
	private Double taxRate = null;
	private Double taxBaseAmount = null;
	private Double taxAmount = null;
	private String currencyCode = null;
	private String generalLedgerCode = null;
	private Long billId = null;
	private Long billRateId = null;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTaxCategory() {
		return taxCategory;
	}

	public void setTaxCategory(String taxCategory) {
		this.taxCategory = taxCategory;
	}

	public String getTaxJurisdiction() {
		return taxJurisdiction;
	}

	public void setTaxJurisdiction(String taxJurisdiction) {
		this.taxJurisdiction = taxJurisdiction;
	}

	public String getTaxType() {
		return taxType;
	}

	public void setTaxType(String taxType) {
		this.taxType = taxType;
	}

	public Double getTaxRate() {
		return taxRate;
	}

	public void setTaxRate(Double taxRate) {
		this.taxRate = taxRate;
	}

	public Double getTaxBaseAmount() {
		return taxBaseAmount;
	}

	public void setTaxBaseAmount(Double taxBaseAmount) {
		this.taxBaseAmount = taxBaseAmount;
	}

	public Double getTaxAmount() {
		return taxAmount;
	}

	public void setTaxAmount(Double taxAmount) {
		this.taxAmount = taxAmount;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public String getGeneralLedgerCode() {
		return generalLedgerCode;
	}

	public void setGeneralLedgerCode(String generalLedgerCode) {
		this.generalLedgerCode = generalLedgerCode;
	}

	public Long getBillId() {
		return billId;
	}

	public void setBillId(Long billId) {
		this.billId = billId;
	}

	public Long getBillRateId() {
		return billRateId;
	}

	public void setBillRateId(Long billRateId) {
		this.billRateId = billRateId;
	}

}
