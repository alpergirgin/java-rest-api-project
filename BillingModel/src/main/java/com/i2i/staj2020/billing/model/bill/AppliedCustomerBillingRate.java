/*
 * API CustomerBill
 *
 * ## TMF API Reference: TMF 678
 *  - Customer bill Management
 *
 * ### Release: 19.5
 *  The Customer Bill Management API allows to find and retrieve one or several customer bills (also called invoices) produced for a customer.
 *  A customer bill is an electronic or paper document produced at the end of the billing process.
 *  The customer bill gathers and displays different items (applied customer billing rates generated during the rating and billing processes) to be charged to a customer.
 *  It represents a total amount due for all the products during the billing period and all significant information like dates, bill reference...
 *  This API provides also operations to find and retrieve the details of applied customer billing rates presented on a customer bill.
 *  Finally, this API allows to request in real-time a customer bill creation and to manage this request.
 *
 * ### Resources
 *  - customerBill
 *  - appliedCustomerBillingRate
 *  - customerBillOnDemand
 *  - billingCycle
 *
 * ### Operations Customer Bill Management API performs the following operations :
 *  - Retrieve a customer bill or a collection of customer bills depending on filter criteria.
 *  - Partial update of  a customer bill (for administration purposes).
 *  - Retrieve an applied customer billing rate or a collection of applied customer billing rates depending on filter criteria.
 *  - Create a customer bill on demand request, retrieve one or a collection of  customer bill on demand request(s) depending on filter criteria.
 *  - Manage notification of events on customer bill and customer bill on demand.
 *
 *  Copyright © TM Forum 2018. All Rights Reserved.
 *
 * OpenAPI spec version: 4.0
 *
 */
package com.i2i.staj2020.billing.model.bill;

import java.sql.Timestamp;
import java.util.List;

/**
 * A customer bill displays applied billing rates created before or during the
 * billing process.
 */
public class AppliedCustomerBillingRate {
	private Long id = null;
	private Long billId = null;
	private String name = null;
	private String description = null;
	private String isBilled = null;
	private Timestamp chargeDate = null;
	private String rateType = null;
	private String baseRateType = null;
	private String chargeType = null;
	private String recordType = null;
	private Double taxExcludedAmount = null;
	private Double taxIncludedAmount = null;
	private Double quantity = null;
	private String taxJurisdiction = null;
	private String taxCategory = null;
	private String generalLedgerCode = null;
	private Long billingAccountId = null;
	private Long productId = null;
	private List<AppliedBillingTaxRate> appliedTax = null;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getBillId() {
		return billId;
	}

	public void setBillId(Long billId) {
		this.billId = billId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getIsBilled() {
		return isBilled;
	}

	public void setIsBilled(String isBilled) {
		this.isBilled = isBilled;
	}

	public Timestamp getChargeDate() {
		return chargeDate;
	}

	public void setChargeDate(Timestamp chargeDate) {
		this.chargeDate = chargeDate;
	}

	public String getRateType() {
		return rateType;
	}

	public void setRateType(String rateType) {
		this.rateType = rateType;
	}

	public String getBaseRateType() {
		return baseRateType;
	}

	public void setBaseRateType(String baseRateType) {
		this.baseRateType = baseRateType;
	}

	public String getChargeType() {
		return chargeType;
	}

	public void setChargeType(String chargeType) {
		this.chargeType = chargeType;
	}

	public String getRecordType() {
		return recordType;
	}

	public void setRecordType(String recordType) {
		this.recordType = recordType;
	}

	public Double getTaxExcludedAmount() {
		return taxExcludedAmount;
	}

	public void setTaxExcludedAmount(Double taxExcludedAmount) {
		this.taxExcludedAmount = taxExcludedAmount;
	}

	public Double getTaxIncludedAmount() {
		return taxIncludedAmount;
	}

	public void setTaxIncludedAmount(Double taxIncludedAmount) {
		this.taxIncludedAmount = taxIncludedAmount;
	}

	public Double getQuantity() {
		return quantity;
	}

	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}

	public String getTaxJurisdiction() {
		return taxJurisdiction;
	}

	public void setTaxJurisdiction(String taxJurisdiction) {
		this.taxJurisdiction = taxJurisdiction;
	}

	public String getTaxCategory() {
		return taxCategory;
	}

	public void setTaxCategory(String taxCategory) {
		this.taxCategory = taxCategory;
	}

	public String getGeneralLedgerCode() {
		return generalLedgerCode;
	}

	public void setGeneralLedgerCode(String generalLedgerCode) {
		this.generalLedgerCode = generalLedgerCode;
	}

	public Long getBillingAccountId() {
		return billingAccountId;
	}

	public void setBillingAccountId(Long billingAccountId) {
		this.billingAccountId = billingAccountId;
	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public List<AppliedBillingTaxRate> getAppliedTax() {
		return appliedTax;
	}

	public void setAppliedTax(List<AppliedBillingTaxRate> appliedTax) {
		this.appliedTax = appliedTax;
	}

}
