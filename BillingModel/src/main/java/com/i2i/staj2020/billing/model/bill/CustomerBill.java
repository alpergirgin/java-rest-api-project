package com.i2i.staj2020.billing.model.bill;

import java.sql.Timestamp;
import java.util.List;

/**
 * The billing account receives all charges (recurring, one time and usage) of
 * the offers and products assigned to it during order process. Periodically
 * according to billing cycle specifications attached to the billing account or
 * as a result of an event, a customer bill (aka invoice) is produced. This
 * customer bill concerns different related parties which play a role on it :
 * for example, a customer bill is produced by an operator, is sent to a bill
 * receiver and has to be paid by a payer. A payment method could be assigned to
 * the customer bill to build the call of payment. Lettering process enables to
 * assign automatically or manually incoming amount from payments to customer
 * bills (payment items). A tax item is created for each tax rate used in the
 * customer bill. The financial account represents a financial entity which
 * records all customer’s accounting events : payment amount are recorded as
 * credit and invoices amount are recorded as debit. It gives the customer
 * overall balance (account balance). The customer bill is linked to one or more
 * documents that can be downloaded via a provided url.
 */
public class CustomerBill {
	private Long id = null;
	private Long billNo = null;
	private String category = null;
	private String runType = null;
	private String currencyCode = null;
	private Timestamp paymentDueDate = null;
	private Double amountDue = null;
	private Double remainingAmount = null;
	private Double taxExcludedAmount = null;
	private Double taxIncludedAmount = null;
	private Long billingAccountId = null;
	private Timestamp billDate = null;
	private Timestamp lastUpdate = null;
	private Timestamp nextBillDate = null;
	private List<AppliedBillingTaxRate> taxItem = null;

	public CustomerBill() {
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getBillNo() {
		return billNo;
	}

	public void setBillNo(Long billNo) {
		this.billNo = billNo;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getRunType() {
		return runType;
	}

	public void setRunType(String runType) {
		this.runType = runType;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public Timestamp getPaymentDueDate() {
		return paymentDueDate;
	}

	public void setPaymentDueDate(Timestamp paymentDueDate) {
		this.paymentDueDate = paymentDueDate;
	}

	public Double getAmountDue() {
		return amountDue;
	}

	public void setAmountDue(Double amountDue) {
		this.amountDue = amountDue;
	}

	public Double getRemainingAmount() {
		return remainingAmount;
	}

	public void setRemainingAmount(Double remainingAmount) {
		this.remainingAmount = remainingAmount;
	}

	public Double getTaxExcludedAmount() {
		return taxExcludedAmount;
	}

	public void setTaxExcludedAmount(Double taxExcludedAmount) {
		this.taxExcludedAmount = taxExcludedAmount;
	}

	public Double getTaxIncludedAmount() {
		return taxIncludedAmount;
	}

	public void setTaxIncludedAmount(Double taxIncludedAmount) {
		this.taxIncludedAmount = taxIncludedAmount;
	}

	public Long getBillingAccountId() {
		return billingAccountId;
	}

	public void setBillingAccountId(Long billingAccountId) {
		this.billingAccountId = billingAccountId;
	}

	public Timestamp getBillDate() {
		return billDate;
	}

	public void setBillDate(Timestamp billDate) {
		this.billDate = billDate;
	}

	public Timestamp getLastUpdate() {
		return lastUpdate;
	}

	public void setLastUpdate(Timestamp lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

	public Timestamp getNextBillDate() {
		return nextBillDate;
	}

	public void setNextBillDate(Timestamp nextBillDate) {
		this.nextBillDate = nextBillDate;
	}

	public List<AppliedBillingTaxRate> getTaxItem() {
		return taxItem;
	}

	public void setTaxItem(List<AppliedBillingTaxRate> taxItem) {
		this.taxItem = taxItem;
	}

}
