package com.i2i.staj2020.billing.manager;

import java.sql.Connection;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

public class ConnectionManager {

//	private static final String HOST = "balarama.db.elephantsql.com";
//	private static final String DB_NAME = "plslmwpc";
//	private static final String USER = "plslmwpc";
//	private static final String PASSWORD = "l1t6HfWQv_xSSQ1nJMRzDDbjH_cVkdCk";
//	private static final String SCHEMA = "BILLING";

	private static final String HOST = "ec2-54-247-79-178.eu-west-1.compute.amazonaws.com";
	private static final String DB_NAME = "d24gfh10jvv4a5";
	private static final String USER = "zvhediiftpdlwc";
	private static final String PASSWORD = "c77ac65fcf35d0878945d47e69cbf64fdf4cf590084384fe3a6133b41fb3a4d7";
	private static final String SCHEMA = "billing";

	private static Logger logger = LoggerFactory.getLogger(ConnectionManager.class);

	private static HikariConfig hikariConfig;
	private static HikariDataSource hikariDataSource;

	private ConnectionManager() {
	}

	/**
	 * Initializes the pool.
	 */
	public static void configure(String applicationName) {

		if (hikariConfig != null && hikariDataSource != null) {
			return;
		}

		hikariConfig = new HikariConfig();
		hikariConfig.setJdbcUrl(String.format("jdbc:postgresql://%s:5432/%s", HOST, DB_NAME));
		hikariConfig.setUsername(USER);
		hikariConfig.setPassword(PASSWORD);
		hikariConfig.setSchema(SCHEMA);
		hikariConfig.setAutoCommit(false);
		hikariConfig.setMaximumPoolSize(1);
		hikariConfig.setConnectionInitSql("SET APPLICATION_NAME TO '" + applicationName + "'");

		hikariDataSource = new HikariDataSource(hikariConfig);
	}

	public static void stop() {
		hikariDataSource.close();
		hikariDataSource = null;
		hikariConfig = null;
	}

	/**
	 * Gets a connection ready to use.
	 * 
	 * @return
	 * @throws SQLException
	 */
	public static Connection getConnection() throws SQLException {
		if (hikariDataSource == null) {
			throw new NullPointerException("You should first configure the manager!");
		}
		return hikariDataSource.getConnection();
	}

	/**
	 * Releases given connection.
	 * 
	 * @param connection
	 */
	public static void releaseConnection(Connection connection) {
		// when using try(connection), already releases the connection
		try {
			if (connection != null && !connection.isClosed()) {
				connection.close();
			}
		} catch (final SQLException e) {
			logger.error(e.getMessage(), e);
		}
	}
}
