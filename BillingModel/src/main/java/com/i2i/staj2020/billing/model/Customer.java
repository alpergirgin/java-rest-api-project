package com.i2i.staj2020.billing.model;

/**
 * Customer information holder
 *
 */
public class Customer {

	private Long id;
	private Long customerNo;
	private String name;
	private String type;
	private Long parentCustomerId;

	public Customer() {

	}

	public Customer(Long id, Long customerNo, String name, String type, Long parentCustomerId) {
		super();
		this.id = id;
		this.customerNo = customerNo;
		this.name = name;
		this.type = type;
		this.parentCustomerId = parentCustomerId;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getCustomerNo() {
		return customerNo;
	}

	public void setCustomerNo(Long customerNo) {
		this.customerNo = customerNo;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Long getParentCustomerId() {
		return parentCustomerId;
	}

	public void setParentCustomerId(Long parentCustomerId) {
		this.parentCustomerId = parentCustomerId;
	}

}
