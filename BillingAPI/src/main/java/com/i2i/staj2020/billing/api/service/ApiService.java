package com.i2i.staj2020.billing.api.service;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.i2i.staj2020.billing.api.dao.BillingAccountDao;
import com.i2i.staj2020.billing.api.dao.CustomerDao;
import com.i2i.staj2020.billing.api.dao.ProductDao;
import com.i2i.staj2020.billing.api.dao.TariffDao;
import com.i2i.staj2020.billing.manager.ConnectionManager;
import com.i2i.staj2020.billing.model.BillingAccount;
import com.i2i.staj2020.billing.model.Customer;
import com.i2i.staj2020.billing.model.Product;
import com.i2i.staj2020.billing.model.Tariff;

public class ApiService {

	private final BillingAccountDao billingAccountDao;
	private final Logger logger = LoggerFactory.getLogger(ApiService.class);
	private final TariffDao tariffDao;
	private final ProductDao productDao;
	private final CustomerDao customerDao;

	public ApiService() {
		ConnectionManager.configure("BillingAPI");
		this.billingAccountDao = new BillingAccountDao();
		this.productDao = new ProductDao();
		this.tariffDao = new TariffDao();
		this.customerDao = new CustomerDao();
	}

	/**
	 * Returns all billing accounts
	 * 
	 * @return BillingAccount List
	 * 
	 */
	public List<BillingAccount> getAllBillingAccounts() {
		List<BillingAccount> billingAccountList = new LinkedList<BillingAccount>();
		try (Connection connection = ConnectionManager.getConnection()) {
			billingAccountList = billingAccountDao.get(connection);
			connection.commit();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return billingAccountList;
	}

	public BillingAccount getBillingAccount(Long id) {
		BillingAccount billingAccount = new BillingAccount();
		try (Connection connection = ConnectionManager.getConnection()) {
			billingAccount = billingAccountDao.get(connection, id);
			connection.commit();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return billingAccount;
	}

	public void insertBillingAccount(BillingAccount billingAccount) {
		try (Connection connection = ConnectionManager.getConnection()) {
			billingAccountDao.insert(connection, billingAccount);
			connection.commit();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void updateBillingAccount(BillingAccount billingAccount) {
		try (Connection connection = ConnectionManager.getConnection()) {
			billingAccountDao.update(connection, billingAccount);
			connection.commit();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void deleteBillingAccount(long id) {
		try (Connection connection = ConnectionManager.getConnection()) {
			billingAccountDao.delete(connection, id);
			connection.commit();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}


	public List<Tariff> getAllTariff() {
		List<Tariff> tariffList = new LinkedList<Tariff>();
		try (Connection connection = ConnectionManager.getConnection()) {
			tariffList = tariffDao.get(connection);
			connection.commit();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return tariffList;
	}
	
	public Tariff getTariff(long id) {
		Tariff tariff = new Tariff();
		try (Connection connection = ConnectionManager.getConnection()) {
			tariff = tariffDao.get(connection, id);
			connection.commit();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return tariff;
	}
	
	public void insertTariff(Tariff tariff) {
		try (Connection connection = ConnectionManager.getConnection()) {
			tariffDao.insert(connection, tariff);
			connection.commit();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	
	public void updateTariff(Tariff tariff) {
		try (Connection connection = ConnectionManager.getConnection()) {
			tariffDao.update(connection, tariff);
			connection.commit();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void deleteTariff(long id) {
		try (Connection connection = ConnectionManager.getConnection()) {
			tariffDao.delete(connection, id);
			connection.commit();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	
	public List<Product> getAllProduct() {
		List<Product> productList = new LinkedList<Product>();
		try (Connection connection = ConnectionManager.getConnection()) {
			productList = productDao.get(connection);
			connection.commit();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return productList;

	}
	
	public Product getProduct(long id) {
		
		Product product = new Product();
		try (Connection connection = ConnectionManager.getConnection()) {
			product = productDao.get(connection, id);
			connection.commit();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return product;
		
		
	}

	public void insertProduct(Product product) {
		try (Connection connection = ConnectionManager.getConnection()) {
			productDao.insert(connection, product);
			connection.commit();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

	public void updateProduct(Product product) {
		try (Connection connection = ConnectionManager.getConnection()) {
			productDao.update(connection, product);
			connection.commit();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

	public void deleteProduct(long id) {
		try (Connection connection = ConnectionManager.getConnection()) {
			productDao.delete(connection, id);
			connection.commit();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

	public List<Customer> getAllCustomer() {
		List<Customer> customerList = new LinkedList<Customer>();
		try (Connection connection = ConnectionManager.getConnection()) {
			customerList = customerDao.get(connection);
			connection.commit();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return customerList;
	}
	
	public Customer getCustomer(long id) {
		Customer customer = new Customer();
		try (Connection connection = ConnectionManager.getConnection()) {
			customer = customerDao.get(connection, id);
			connection.commit();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return customer;
	}
	
	public void insertCustomer(Customer customer) {
		try (Connection connection = ConnectionManager.getConnection()) {
			customerDao.insert(connection, customer);
			connection.commit();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	
	public void updateCustomer(Customer customer) {
		try (Connection connection = ConnectionManager.getConnection()) {
			customerDao.update(connection, customer);
			connection.commit();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void deleteCustomer(long id) {
		try (Connection connection = ConnectionManager.getConnection()) {
			customerDao.delete(connection, id);
			connection.commit();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
