package com.i2i.staj2020.billing.api.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.i2i.staj2020.billing.api.service.ApiService;
import com.i2i.staj2020.billing.model.Product;

@RequestMapping("/api/product")
@RestController
public class ProductController {
	
private final ApiService apiService;
	
	ProductController() {
		apiService = new ApiService();
	}
	
	@GetMapping("/all")
	public ResponseEntity<List<Product>> all() {

		List<Product> productList = new ArrayList<Product>();
		productList = apiService.getAllProduct();

		return new ResponseEntity<List<Product>>(productList, HttpStatus.OK);
	}

	@GetMapping("/{id}")
	public ResponseEntity<Product> one(@PathVariable Long id) {

		Product product = apiService.getProduct(id);

		return new ResponseEntity<Product>(product, HttpStatus.OK);
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<Product> delete(@PathVariable Long id){
		apiService.deleteProduct(id);

		return ResponseEntity.noContent().build();
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<Product> update(@RequestBody Product product){
		
		apiService.updateProduct(product);
		return new ResponseEntity<Product>(product, HttpStatus.OK);
		
	}	
	
	@PostMapping
	public ResponseEntity<Product> insert(@RequestBody Product product){
		
		apiService.insertProduct(product);
		return new ResponseEntity<Product>(product, HttpStatus.OK);
		
	}
}
