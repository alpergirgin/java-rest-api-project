package com.i2i.staj2020.billing.api.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.i2i.staj2020.billing.api.service.ApiService;
import com.i2i.staj2020.billing.model.BillingAccount;

@RequestMapping("/api/billingAccount")
@RestController
public class BillingAccountController {

	private final ApiService apiService;
	
	/*
	 * Aşağıdaki linkte anlatılan ornek baz alınarak geliştirme yapıldı.
	 * Tek fark olarak donen data Resource objeji yerine ResponseEntity kullanıldı.
	 * https://spring.io/guides/tutorials/bookmarks/
	 */

	BillingAccountController() {
		apiService = new ApiService();
	}

	
	@GetMapping("/all")
	public ResponseEntity<List<BillingAccount>> all() {

		List<BillingAccount> billingAccountList = new ArrayList<BillingAccount>();
		billingAccountList = apiService.getAllBillingAccounts();

		return new ResponseEntity<List<BillingAccount>>(billingAccountList, HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<BillingAccount> one(@PathVariable Long id) {

		BillingAccount billingAccount = apiService.getBillingAccount(id);

		return new ResponseEntity<BillingAccount>(billingAccount, HttpStatus.OK);
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<BillingAccount> delete(@PathVariable Long id){
		apiService.deleteBillingAccount(id);

		return ResponseEntity.noContent().build();
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<BillingAccount> update(@RequestBody BillingAccount billingAccount){
		
		apiService.updateBillingAccount(billingAccount);
		return new ResponseEntity<BillingAccount>(billingAccount, HttpStatus.OK);
		
	}	
	
	@PostMapping
	public ResponseEntity<BillingAccount> insert(@RequestBody BillingAccount billingAccount){
		
		apiService.insertBillingAccount(billingAccount);
		return new ResponseEntity<BillingAccount>(billingAccount, HttpStatus.OK);
		
	}
}