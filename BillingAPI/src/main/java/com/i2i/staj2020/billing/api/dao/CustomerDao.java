package com.i2i.staj2020.billing.api.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import com.i2i.staj2020.billing.api.cache.SQLCache;
import com.i2i.staj2020.billing.model.Customer;

public class CustomerDao {
	public List<Customer> get(Connection connection) {
		List<Customer> customerList = new LinkedList<Customer>();
		
		String query = SQLCache.SELECT.ALL_CUSTOMER;
		try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
			try (ResultSet resultSet = preparedStatement.executeQuery()) {
				while (resultSet.next()) {
					Customer customer = new Customer();
					customer.setId(resultSet.getLong("ID"));
					customer.setCustomerNo(resultSet.getLong("CUSTOMER_NO"));
					customer.setName(resultSet.getString("NAME"));
					customer.setType(resultSet.getString("TYPE"));
					customer.setParentCustomerId(resultSet.getLong("PARENT_CUSTOMER_ID"));
					customerList.add(customer);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return customerList;
		}

		public Customer get(Connection connection, Long customerId) {
			Customer customer = new Customer();

			String query = SQLCache.SELECT.CUSTOMER;
			try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
				preparedStatement.setLong(1, customerId);

				try (ResultSet resultSet = preparedStatement.executeQuery()) {
					if (resultSet.next()) {
						customer.setId(resultSet.getLong("ID"));
						customer.setCustomerNo(resultSet.getLong("CUSTOMER_NO"));
						customer.setName(resultSet.getString("NAME"));
						customer.setType(resultSet.getString("TYPE"));
						customer.setParentCustomerId(resultSet.getLong("PARENT_CUSTOMER_ID"));
					}
				}

			} catch (SQLException e) {
				e.printStackTrace();
			}

			return customer;
		}

		public void update(Connection connection,Customer customer) {
			try (PreparedStatement preparedStatement = connection.prepareStatement(SQLCache.UPDATE.CUSTOMER)) {	
				preparedStatement.setLong(1, customer.getCustomerNo());
				preparedStatement.setString(2, customer.getName());
				preparedStatement.setString(3, customer.getType());
				preparedStatement.setLong(4, customer.getParentCustomerId());
				preparedStatement.setLong(5, customer.getId());
				preparedStatement.executeUpdate();
			} catch (SQLException e) {
				e.printStackTrace();
			}

		}

		public void insert(Connection connection, Customer customer) {
			try (PreparedStatement preparedStatement = connection.prepareStatement(SQLCache.INSERT.CUSTOMER)) {
				preparedStatement.setLong(1, customer.getCustomerNo());
				preparedStatement.setString(2, customer.getName());
				preparedStatement.setString(3, customer.getType());
				preparedStatement.setLong(4, customer.getParentCustomerId());				
				preparedStatement.executeUpdate();
			} catch (SQLException e) {
				e.printStackTrace();
			}

		}
		public void delete(Connection connection, long id) {
			try (PreparedStatement preparedStatement = connection.prepareStatement(SQLCache.DELETE.CUSTOMER)) {
				preparedStatement.setLong(1, id);
				preparedStatement.executeUpdate();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

}
