package com.i2i.staj2020.billing.api.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.i2i.staj2020.billing.api.service.ApiService;
import com.i2i.staj2020.billing.model.Tariff;

@RequestMapping("/api/tariff")
@RestController
public class TariffController {
	
	private final ApiService apiService;
	
	TariffController() {
		apiService = new ApiService();
	}
	
	@GetMapping("/all")
	public ResponseEntity<List<Tariff>> all() {

		List<Tariff> tariffList = new ArrayList<Tariff>();
		tariffList = apiService.getAllTariff();

		return new ResponseEntity<List<Tariff>>(tariffList, HttpStatus.OK);
	}

	@GetMapping("/{id}")
	public ResponseEntity<Tariff> one(@PathVariable Long id) {

		Tariff tariff = apiService.getTariff(id);

		return new ResponseEntity<Tariff>(tariff, HttpStatus.OK);
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Tariff> delete(@PathVariable Long id){
		apiService.deleteTariff(id);

		return ResponseEntity.noContent().build();
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<Tariff> update(@RequestBody Tariff tariff){
		
		apiService.updateTariff(tariff);
		return new ResponseEntity<Tariff>(tariff, HttpStatus.OK);
		
	}	
	
	@PostMapping
	public ResponseEntity<Tariff> insert(@RequestBody Tariff tariff){
		
		apiService.insertTariff(tariff);
		return new ResponseEntity<Tariff>(tariff, HttpStatus.OK);
		
	}

}
