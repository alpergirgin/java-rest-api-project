package com.i2i.staj2020.billing.api.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.LinkedList;
import java.util.List;

import com.i2i.staj2020.billing.api.cache.SQLCache;
import com.i2i.staj2020.billing.model.BillingAccount;

/**
 * Billing Data Access Object
 * 
 * @author Mustafa Yusufoglu
 *
 */
public class BillingAccountDao {

	public List<BillingAccount> get(Connection connection) {
		List<BillingAccount> billingAccountList = new LinkedList<BillingAccount>();

		String query = SQLCache.SELECT.ALL_BILLING_ACCOUNTS;
		try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
			try (ResultSet resultSet = preparedStatement.executeQuery()) {
				while (resultSet.next()) {
					BillingAccount billingAccount = new BillingAccount();
					billingAccount.setId(resultSet.getLong("ID"));
					billingAccount.setName(resultSet.getString("NAME"));
					billingAccount.setCustomerId(resultSet.getLong("CUSTOMER_ID"));
					billingAccount.setBillingAccountNo(resultSet.getLong("BILLING_ACCOUNT_NO"));
					billingAccount.setCurrency(resultSet.getString("CURRENCY"));
					billingAccount.setTaxJurisdiction(resultSet.getString("TAX_JURISDICTION"));
					billingAccount.setLastBillDate(resultSet.getTimestamp("LASTBILLDATE"));
					billingAccount.setBillCycle(resultSet.getInt("BILL_CYCLE"));
					billingAccountList.add(billingAccount);

					// Veritabanından verilerin çekilme işlemi yapıldı

				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return billingAccountList;

	}

	public BillingAccount get(Connection connection, Long billingAccountId) {
		BillingAccount billingAccount = new BillingAccount();

		String query = SQLCache.SELECT.BILLING_ACCOUNT;
		try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
			preparedStatement.setLong(1, billingAccountId);

			try (ResultSet resultSet = preparedStatement.executeQuery()) {
				if (resultSet.next()) {
					billingAccount.setId(resultSet.getLong("ID"));
					billingAccount.setName(resultSet.getString("NAME"));
					billingAccount.setCustomerId(resultSet.getLong("CUSTOMER_ID"));
					billingAccount.setBillingAccountNo(resultSet.getLong("BILLING_ACCOUNT_NO"));
					billingAccount.setCurrency(resultSet.getString("CURRENCY"));
					billingAccount.setTaxJurisdiction(resultSet.getString("TAX_JURISDICTION"));
					billingAccount.setLastBillDate(resultSet.getTimestamp("LASTBILLDATE"));
					billingAccount.setBillCycle(resultSet.getInt("BILL_CYCLE"));
				}
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return billingAccount;
	}

	public void update(Connection connection, BillingAccount billingAccount) {
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQLCache.UPDATE.BILLING_ACCOUNT)) {
			
			preparedStatement.setString(1, billingAccount.getName());
			preparedStatement.setLong(2, billingAccount.getCustomerId());
			preparedStatement.setString(3, "everyyy@body.com");
			if (billingAccount.getBillingAccountNo() == null) {
				preparedStatement.setNull(4, Types.NUMERIC);
			} else {
				preparedStatement.setLong(4, billingAccount.getBillingAccountNo());
			}
			preparedStatement.setTimestamp(5, billingAccount.getLastBillDate());
			preparedStatement.setString(6, billingAccount.getCurrency());
			preparedStatement.setString(7, billingAccount.getTaxJurisdiction());
			preparedStatement.setLong(8, billingAccount.getBillCycle());
			preparedStatement.setLong(9,billingAccount.getId());
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	public void insert(Connection connection, BillingAccount billingAccount) {
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQLCache.INSERT.BILLING_ACCOUNT)) {

			preparedStatement.setString(1, billingAccount.getName());
			preparedStatement.setLong(2, billingAccount.getCustomerId());
			preparedStatement.setString(3, "any@body.com");
			if (billingAccount.getBillingAccountNo() == null) {
				preparedStatement.setNull(4, Types.NUMERIC);
			} else {
				preparedStatement.setLong(4, billingAccount.getBillingAccountNo());
			}
			preparedStatement.setTimestamp(5, billingAccount.getLastBillDate());
			preparedStatement.setString(6, billingAccount.getCurrency());
			preparedStatement.setString(7, billingAccount.getTaxJurisdiction());
			preparedStatement.setLong(8, billingAccount.getBillCycle());
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	public void delete(Connection connection, long id) {
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQLCache.DELETE.BILLING_ACCOUNT)) {
			preparedStatement.setLong(1, id);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
