package com.i2i.staj2020.billing.api.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import com.i2i.staj2020.billing.api.cache.SQLCache;
import com.i2i.staj2020.billing.model.Product;

public class ProductDao {
	
	public List<Product> get(Connection connection) {
		List<Product> productList = new LinkedList<Product>();
		
		String query = SQLCache.SELECT.ALL_PRODUCT;
		try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
			try (ResultSet resultSet = preparedStatement.executeQuery()) {
				while (resultSet.next()) {
					Product product = new Product();
					product.setId(resultSet.getLong("ID"));
					product.setName(resultSet.getString("NAME"));
					product.setParentId(resultSet.getLong("PARENT_ID"));
					product.setType(resultSet.getInt("TYPE"));
					product.setBillingAccountId(resultSet.getLong("BILLING_ACCOUNT_ID"));
					product.setTariffId(resultSet.getLong("TARIFF_ID"));
					
					productList.add(product);
					
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return productList;
		}
	
	public Product get(Connection connection, Long productId) {
		Product product = new Product();

		String query = SQLCache.SELECT.PRODUCT;
		try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
			preparedStatement.setLong(1, productId);

			try (ResultSet resultSet = preparedStatement.executeQuery()) {
				if (resultSet.next()) {
					product.setId(resultSet.getLong("ID"));
					product.setName(resultSet.getString("NAME"));
					product.setParentId(resultSet.getLong("PARENT_ID"));
					product.setType(resultSet.getInt("TYPE"));
					product.setBillingAccountId(resultSet.getLong("BILLING_ACCOUNT_ID"));
					product.setTariffId(resultSet.getLong("TARIFF_ID"));
				}
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return product;
	}

	public void update(Connection connection, Product product) {
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQLCache.UPDATE.PRODUCT)) {	
			preparedStatement.setString(1, product.getName());	
			preparedStatement.setLong(2, product.getParentId());
			preparedStatement.setInt(3, product.getType());
			preparedStatement.setLong(4, product.getBillingAccountId());
			preparedStatement.setLong(5, product.getTariffId());
			preparedStatement.setLong(6, product.getId());
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	public void insert(Connection connection, Product product) {
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQLCache.INSERT.PRODUCT)) {
			preparedStatement.setString(1, product.getName());	
			preparedStatement.setLong(2, product.getParentId());
			preparedStatement.setInt(3, product.getType());
			preparedStatement.setLong(4, product.getBillingAccountId());
			preparedStatement.setLong(5, product.getTariffId());
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}
	public void delete(Connection connection, long id) {
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQLCache.DELETE.PRODUCT)) {
			preparedStatement.setLong(1, id);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
			
			
}
