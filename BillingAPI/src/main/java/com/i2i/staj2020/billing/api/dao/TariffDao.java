package com.i2i.staj2020.billing.api.dao;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;

import com.i2i.staj2020.billing.api.cache.SQLCache;
import com.i2i.staj2020.billing.model.Tariff;

public class TariffDao {	

	public List<Tariff> get(Connection connection) {
		List<Tariff> tariffList = new LinkedList<Tariff>();
		
		String query = SQLCache.SELECT.ALL_TARIFF;
		try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
			try (ResultSet resultSet = preparedStatement.executeQuery()) {
				while (resultSet.next()) {
					Tariff tariff = new Tariff();
					tariff.setId(resultSet.getLong("ID"));
					tariff.setName(resultSet.getString("NAME"));
					tariff.setPrice(resultSet.getDouble("PRICE"));
					tariffList.add(tariff);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return tariffList;
		}

		public Tariff get(Connection connection, Long tariffId) {
			Tariff tariff = new Tariff();

			String query = SQLCache.SELECT.TARIFF;
			try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
				preparedStatement.setLong(1, tariffId);

				try (ResultSet resultSet = preparedStatement.executeQuery()) {
					if (resultSet.next()) {
						tariff.setId(resultSet.getLong("ID"));
						tariff.setName(resultSet.getString("NAME"));
						tariff.setPrice(resultSet.getDouble("PRICE"));
					}
				}

			} catch (SQLException e) {
				e.printStackTrace();
			}

			return tariff;
		}

		public void update(Connection connection, Tariff tariff) {
			try (PreparedStatement preparedStatement = connection.prepareStatement(SQLCache.UPDATE.TARIFF)) {
				
				preparedStatement.setDouble(1, tariff.getPrice());
				preparedStatement.setString(2, tariff.getName());	
				preparedStatement.setLong(3, tariff.getId());
				preparedStatement.executeUpdate();
			} catch (SQLException e) {
				e.printStackTrace();
			}

		}

		public void insert(Connection connection, Tariff tariff) {
			try (PreparedStatement preparedStatement = connection.prepareStatement(SQLCache.INSERT.TARIFF)) {
				preparedStatement.setDouble(1, tariff.getPrice());
				preparedStatement.setString(2, tariff.getName());				
				preparedStatement.executeUpdate();
			} catch (SQLException e) {
				e.printStackTrace();
			}

		}
		public void delete(Connection connection, long id) {
			try (PreparedStatement preparedStatement = connection.prepareStatement(SQLCache.DELETE.TARIFF)) {
				preparedStatement.setLong(1, id);
				preparedStatement.executeUpdate();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
