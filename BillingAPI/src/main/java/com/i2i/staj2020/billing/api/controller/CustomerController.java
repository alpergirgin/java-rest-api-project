package com.i2i.staj2020.billing.api.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.i2i.staj2020.billing.api.service.ApiService;
import com.i2i.staj2020.billing.model.Customer;

@RequestMapping("/api/customer")
@RestController
public class CustomerController {

private final ApiService apiService;
	
	CustomerController() {
		apiService = new ApiService();
	}
	
	@GetMapping("/all")
	public ResponseEntity<List<Customer>> all() {

		List<Customer> customerList = new ArrayList<Customer>();
		customerList = apiService.getAllCustomer();

		return new ResponseEntity<List<Customer>>(customerList, HttpStatus.OK);
	}

	@GetMapping("/{id}")
	public ResponseEntity<Customer> one(@PathVariable Long id) {

		Customer customer = apiService.getCustomer(id);

		return new ResponseEntity<Customer>(customer, HttpStatus.OK);
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Customer> delete(@PathVariable Long id){
		apiService.deleteCustomer(id);

		return ResponseEntity.noContent().build();
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<Customer> update(@RequestBody Customer customer){
		
		apiService.updateCustomer(customer);
		return new ResponseEntity<Customer>(customer, HttpStatus.OK);
		
	}	
	
	@PostMapping
	public ResponseEntity<Customer> insert(@RequestBody Customer customer){
		
		apiService.insertCustomer(customer);
		return new ResponseEntity<Customer>(customer, HttpStatus.OK);		
	}
	
}
