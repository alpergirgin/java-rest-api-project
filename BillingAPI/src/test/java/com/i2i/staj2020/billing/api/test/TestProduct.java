package com.i2i.staj2020.billing.api.test;

import java.util.List;

import org.junit.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.i2i.staj2020.billing.api.service.ApiService;
import com.i2i.staj2020.billing.model.Product;




public class TestProduct {

	ApiService apiService = new ApiService();
	
	//CRUD İslemleri
	
	@Test
	public void testCreate() {
		try {
			Product product = createJson();
			apiService.insertProduct(product);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testReadAll() {
		List<Product> productList = apiService.getAllProduct();

		for (int i = 0; i < productList.size(); i++) {
			Product product = productList.get(i);
			System.out.println(toJson(product));
		}
	}
	
	@Test
	public void testRead() {
		Product product = apiService.getProduct(8L);
		System.out.println(toJson(product));
	}
	
	@Test
	public void testUpdate() {
		try {
			Product product = updateJson();
			apiService.updateProduct(product);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	

	@Test
	public void testDelete() {
		try {
			apiService.deleteProduct(8L);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private String toJson(Product product) {
	
		try {
			JsonMapper mapper = new JsonMapper();
			mapper.enable(SerializationFeature.INDENT_OUTPUT);
			return mapper.writeValueAsString(product);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return null;
		}
	

	private Product createJson() {
		try {
			JsonMapper mapper = new JsonMapper();

			return mapper.readValue(
					"{\"name\" : \"Ali\", \"parentId\" : 160, \"tariffId\" : 4, \"type\" : 4 ,\"billingAccountId\" : 4 }",Product.class);					
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}
	

	private Product updateJson() {
		try {
			JsonMapper mapper = new JsonMapper();

			return mapper.readValue(
					"{\"id\" : 9,\"name\" : \"Batuhan\", \"parentId\" : 160, \"tariffId\" : 4, \"type\" : 4 ,\"billingAccountId\" : 4 }",Product.class);					
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
}
