package com.i2i.staj2020.billing.api.test;

import java.util.List;

import org.junit.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.i2i.staj2020.billing.api.service.ApiService;
import com.i2i.staj2020.billing.model.BillingAccount;

public class TestBillingAccount {

	ApiService apiService = new ApiService();

	// CRUD
	@Test
	public void testCreate() {
		try {
			BillingAccount billingAccount = createJson();
			apiService.insertBillingAccount(billingAccount);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testReadAll() {
		List<BillingAccount> billingAccountList = apiService.getAllBillingAccounts();

		for (int i = 0; i < billingAccountList.size(); i++) {
			BillingAccount billingAccount = billingAccountList.get(i);
			System.out.println(toJson(billingAccount));
		}
	}

	@Test
	public void testRead() {
		BillingAccount billingAccount = apiService.getBillingAccount(4L);
		System.out.println(toJson(billingAccount));
	}

	@Test
	public void testUpdate() {
		try {
			BillingAccount billingAccount = updateJson();
			apiService.updateBillingAccount(billingAccount);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testDelete() {
		try {
			apiService.deleteBillingAccount(14L);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private String toJson(BillingAccount billingAccount) {
		try {
			JsonMapper mapper = new JsonMapper();
			mapper.enable(SerializationFeature.INDENT_OUTPUT);
			return mapper.writeValueAsString(billingAccount);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return null;
	}

	private BillingAccount createJson() {
		try {
			JsonMapper mapper = new JsonMapper();

			return mapper.readValue(
					"{\"billingAccountNo\" : 369,   \"name\" : \"Alper Girgin\",   \"customerId\" : 67,   \"currency\" : \"TRY\","
							+ "   \"billCycle\" : 1,  \"taxJurisdiction\" : \"TR\",  \"lastBillDate\" : null }",
					BillingAccount.class);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	private BillingAccount updateJson() {
		try {
			JsonMapper mapper = new JsonMapper();
			// @formatter:off
			return mapper.readValue(
					"{\"id\" : 4, \"billingAccountNo\" : 7, \"name\" : \"Batuhan Karadeniz\" , \"customerId\" : 7 ,"
					+ "\"currency\":\"AA\", \"billCycle\" : 11, \"taxJurisdiction\":\"EU\" , \"lastBillDate\":null }",BillingAccount.class);
			// @formatter:on
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
