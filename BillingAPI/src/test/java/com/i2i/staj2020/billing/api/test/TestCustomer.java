package com.i2i.staj2020.billing.api.test;

import java.util.List;

import org.junit.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.i2i.staj2020.billing.api.service.ApiService;
import com.i2i.staj2020.billing.model.Customer;


public class TestCustomer {
ApiService apiService = new ApiService();
	
	//CRUD islemleri
	
	@Test
	public void testCreate() {
		try {
			Customer customer = createJson();
			apiService.insertCustomer(customer);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	@Test
	public void testReadAll() {
		List<Customer> customerList = apiService.getAllCustomer();
		for (int i = 0; i < customerList.size(); i++) {
			Customer customer = customerList.get(i);
			System.out.println(toJson(customer));
		}
	}
	@Test
	public void testRead() {
		Customer customer = apiService.getCustomer(3L);
		System.out.println(toJson(customer));
	}
	
	@Test
	public void testUpdate() {
		try {
			Customer customer = updateJson();
			apiService.updateCustomer(customer);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testDelete() {
		try {
			apiService.deleteCustomer(4L);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private String toJson(Customer customer) {
	
		try {
			JsonMapper mapper = new JsonMapper();
			mapper.enable(SerializationFeature.INDENT_OUTPUT);
			return mapper.writeValueAsString(customer);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return null;
		}

	private Customer createJson() {
		try {
			JsonMapper mapper = new JsonMapper();

			return mapper.readValue(
					"{\"customerNo\" : 7,   \"name\" : \"alper\", \"type\" : \"dayi\", \"parentCustomerId\" : 67 }",Customer.class);					
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}
	

	private Customer updateJson() {
		try {
			JsonMapper mapper = new JsonMapper();
			// @formatter:off
			return mapper.readValue(
					"{\"id\" : 5,\"customerNo\" : 9,   \"name\" : \"batuhan\", \"type\" : \"baba\", \"parentCustomerId\" : 42 }",Customer.class);
			// @formatter:on
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
