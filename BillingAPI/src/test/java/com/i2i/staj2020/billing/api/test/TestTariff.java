package com.i2i.staj2020.billing.api.test;

import java.util.List;

import org.junit.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.i2i.staj2020.billing.api.service.ApiService;
import com.i2i.staj2020.billing.model.Tariff;

public class TestTariff {
	
	ApiService apiService = new ApiService();
	
	//CRUD islemleri
	
	@Test
	public void testCreate() {
		try {
			Tariff tariff = createJson();
			apiService.insertTariff(tariff);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	@Test
	public void testReadAll() {
		List<Tariff> tariffList = apiService.getAllTariff();

		for (int i = 0; i < tariffList.size(); i++) {
			Tariff tariff = tariffList.get(i);
			System.out.println(toJson(tariff));
		}
	}
	@Test
	public void testRead() {
		Tariff tariff = apiService.getTariff(423L);
		System.out.println(toJson(tariff));
	}
	
	@Test
	public void testUpdate() {
		try {
			Tariff tariff = updateJson();
			apiService.updateTariff(tariff);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testDelete() {
		try {
			apiService.deleteTariff(4L);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private String toJson(Tariff tariff) {
	
		try {
			JsonMapper mapper = new JsonMapper();
			mapper.enable(SerializationFeature.INDENT_OUTPUT);
			return mapper.writeValueAsString(tariff);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return null;
		}

	private Tariff createJson() {
		try {
			JsonMapper mapper = new JsonMapper();

			return mapper.readValue(
					"{\"price\" : 125,   \"name\" : \"4 gallon\" }",Tariff.class);					
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}
	

	private Tariff updateJson() {
		try {
			JsonMapper mapper = new JsonMapper();
			// @formatter:off
			return mapper.readValue(
					"{\"id\" : 480,\"price\" : 71, \"name\" : \"4 pint\" }",Tariff.class);
			// @formatter:on
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
