package com.i2i.staj2020.billing.engine.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.LinkedList;
import java.util.List;

import com.i2i.staj2020.billing.engine.cache.SQLCache;
import com.i2i.staj2020.billing.model.Tariff;

/**
 * Billing Data Access Object
 * 
 * @author Nida Cikaray
 *
 */
public class TariffDao {
	public List<Tariff> get(Connection connection) {
		List<Tariff> tariffList = new LinkedList<Tariff>();
		String query = SQLCache.SELECT.ALL_TARIFF;
		try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
			try (ResultSet resultSet = preparedStatement.executeQuery()) {
				while (resultSet.next()) {
					Tariff tariff = new Tariff();
					tariff.setId(resultSet.getLong("ID"));
					tariff.setPrice(resultSet.getDouble("PRICE"));
					tariff.setName(resultSet.getString("NAME"));
					
					tariffList.add(tariff);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return tariffList;
	}
	
	public Tariff get(Connection connection, Long id) {
		
		String query = SQLCache.SELECT.TARIFF;
		try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
			preparedStatement.setLong(1,id);
			try (ResultSet resultSet = preparedStatement.executeQuery()) {
				if (resultSet.next()) {
					Tariff tariff = new Tariff();
					tariff.setId(resultSet.getLong("ID"));
					tariff.setPrice(resultSet.getDouble("PRICE"));
					tariff.setName(resultSet.getString("NAME"));
					
					return tariff;
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public void insertTariff(Connection connection, Tariff tariff) {

		try (PreparedStatement preparedStatement = connection.prepareStatement(SQLCache.INSERT.TARIFF)) {

			preparedStatement.setDouble(1, tariff.getPrice());
			preparedStatement.setString(2, tariff.getName());
			preparedStatement.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void update(Connection connection, Tariff tariff) {
		try (PreparedStatement preparedStatement = connection
				.prepareStatement(SQLCache.UPDATE.TARIFF_LAST_BILL_DATE)) {
			// �rnek: preparedStatement.setLong(1, billingAccount.getId());
			Timestamp lastBillDate = new Timestamp(System.currentTimeMillis());
			preparedStatement.setTimestamp(1, lastBillDate);
			preparedStatement.setLong(2, tariff.getId());
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}
}
