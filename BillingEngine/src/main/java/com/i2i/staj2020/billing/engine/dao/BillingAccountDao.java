package com.i2i.staj2020.billing.engine.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.LinkedList;
import java.util.List;

import com.i2i.staj2020.billing.engine.cache.SQLCache;
import com.i2i.staj2020.billing.model.BillingAccount;

/**
 * Billing Data Access Object
 * 
 * @author Mustafa Yusufoglu
 *
 */
public class BillingAccountDao {

	public List<BillingAccount> get(Connection connection) {
		List<BillingAccount> billingAccountList = new LinkedList<BillingAccount>();

		String query = SQLCache.SELECT.ALL_BILLING_ACCOUNTS;
		try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
			try (ResultSet resultSet = preparedStatement.executeQuery()) {
				while (resultSet.next()) {
					BillingAccount billingAccount = new BillingAccount();
					billingAccount.setId(resultSet.getLong("ID"));
					billingAccount.setName(resultSet.getString("NAME"));
					billingAccount.setCustomerId(resultSet.getLong("CUSTOMER_ID"));
					billingAccountList.add(billingAccount);
				}
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return billingAccountList;

	}

	public void update(Connection connection, BillingAccount billingAccount) {
		try (PreparedStatement preparedStatement = connection
				.prepareStatement(SQLCache.UPDATE.BILLING_ACCOUNT_LAST_BILL_DATE)) {
			Timestamp lastBillDate = new Timestamp(System.currentTimeMillis());
			preparedStatement.setTimestamp(1, lastBillDate);
			preparedStatement.setLong(2, billingAccount.getId());
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

}
