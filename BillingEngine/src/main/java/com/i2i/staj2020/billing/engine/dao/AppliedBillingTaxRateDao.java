package com.i2i.staj2020.billing.engine.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.LinkedList;
import java.util.List;

import com.i2i.staj2020.billing.engine.cache.SQLCache;
import com.i2i.staj2020.billing.model.bill.AppliedBillingTaxRate;

public class AppliedBillingTaxRateDao {
	public List<AppliedBillingTaxRate> get(Connection connection) {
		List<AppliedBillingTaxRate> appliedBillingTaxRateList = new LinkedList<AppliedBillingTaxRate>();
		String query = SQLCache.SELECT.ALL_APPLIED_BILLING_TAX_RATE;
		try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
			try (ResultSet resultSet = preparedStatement.executeQuery()) {
				while (resultSet.next()) {
					AppliedBillingTaxRate appliedBillingTaxRate = new AppliedBillingTaxRate();
					appliedBillingTaxRate.setId(resultSet.getLong("ID"));
					appliedBillingTaxRate.setTaxCategory(resultSet.getString("TAX_CATEGORY"));
					appliedBillingTaxRate.setTaxJurisdiction(resultSet.getString("TAX_JURISDICTION"));
					appliedBillingTaxRate.setTaxType(resultSet.getString("TAX_TYPE"));
					appliedBillingTaxRate.setTaxRate(resultSet.getDouble("TAX_RATE"));
					appliedBillingTaxRate.setTaxBaseAmount(resultSet.getDouble("TAX_BASE_AMOUNT"));
					appliedBillingTaxRate.setTaxAmount(resultSet.getDouble("TAX_AMOUNT"));
					appliedBillingTaxRate.setCurrencyCode(resultSet.getString("CURRENCY_CODE"));
					appliedBillingTaxRate.setGeneralLedgerCode(resultSet.getString("GENERAL_LEDGER_CODE"));
					appliedBillingTaxRate.setBillId(resultSet.getLong("BILL_ID"));
					appliedBillingTaxRate.setBillRateId(resultSet.getLong("BILL_RATE_ID"));
					appliedBillingTaxRateList.add(appliedBillingTaxRate);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return appliedBillingTaxRateList;
	}
	



	public void insertAppliedBillingTaxRate(Connection connection, AppliedBillingTaxRate appliedBillingTaxRate) {

		try (PreparedStatement preparedStatement = connection
				.prepareStatement(SQLCache.INSERT.APPLIED_BILLING_TAX_RATE)) {

			preparedStatement.setString(1, appliedBillingTaxRate.getTaxCategory());
			preparedStatement.setString(2, appliedBillingTaxRate.getTaxJurisdiction());
			preparedStatement.setString(3, appliedBillingTaxRate.getTaxType());
			preparedStatement.setDouble(4, appliedBillingTaxRate.getTaxRate());
			preparedStatement.setDouble(5, appliedBillingTaxRate.getTaxBaseAmount());
			preparedStatement.setDouble(6, appliedBillingTaxRate.getTaxAmount());
			preparedStatement.setString(7, appliedBillingTaxRate.getCurrencyCode());
			preparedStatement.setString(8, appliedBillingTaxRate.getGeneralLedgerCode());
			preparedStatement.setLong(9, appliedBillingTaxRate.getBillId());
			preparedStatement.setLong(10, appliedBillingTaxRate.getBillRateId());
			preparedStatement.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void update(Connection connection, AppliedBillingTaxRate appliedBillingTaxRate) {
		try (PreparedStatement preparedStatement = connection
				.prepareStatement(SQLCache.UPDATE.APPLIED_BILLING_TAX_RATE_LAST_BILL_DATE)) {
			// �rnek: preparedStatement.setLong(1, billingAccount.getId());
			Timestamp lastBillDate = new Timestamp(System.currentTimeMillis());
			preparedStatement.setTimestamp(1, lastBillDate);
			preparedStatement.setLong(2, appliedBillingTaxRate.getId());
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

}
