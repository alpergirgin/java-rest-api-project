package com.i2i.staj2020.billing.engine.service;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.i2i.staj2020.billing.engine.dao.AppliedBillingTaxRateDao;
import com.i2i.staj2020.billing.engine.dao.AppliedCustomerBillingRateDao;
import com.i2i.staj2020.billing.engine.dao.BillingAccountDao;
import com.i2i.staj2020.billing.engine.dao.CustomerBillDao;
import com.i2i.staj2020.billing.engine.dao.ProductDao;
import com.i2i.staj2020.billing.engine.dao.TariffDao;
import com.i2i.staj2020.billing.model.bill.AppliedBillingTaxRate;
import com.i2i.staj2020.billing.model.bill.AppliedCustomerBillingRate;
import com.i2i.staj2020.billing.model.bill.CustomerBill;
import com.i2i.staj2020.billing.manager.ConnectionManager;
import com.i2i.staj2020.billing.model.BillingAccount;
import com.i2i.staj2020.billing.model.Product;
import com.i2i.staj2020.billing.model.Tariff;

public class BillingEngineService {

	private final BillingAccountDao billingAccountDao;
	private final CustomerBillDao customerBillDao;
	private final AppliedCustomerBillingRateDao appliedCustomerBillingRateDao;
	private final AppliedBillingTaxRateDao appliedBillingTaxRateDao;
	private final ProductDao productDao;
	private final TariffDao tariffDao;
	private final Logger logger = LoggerFactory.getLogger(BillingEngineService.class);

	public BillingEngineService() {
		this.billingAccountDao = new BillingAccountDao();
		this.customerBillDao = new CustomerBillDao();
		this.appliedCustomerBillingRateDao = new AppliedCustomerBillingRateDao();
		this.appliedBillingTaxRateDao = new AppliedBillingTaxRateDao();
		this.productDao = new ProductDao();
		this.tariffDao = new TariffDao();
		ConnectionManager.configure("BillingEngine");
	}

	/**
	 * Returns all billing accounts
	 * 
	 * @return BillingAccount List
	 * 
	 */
	public List<BillingAccount> getAllBillingAccounts() {
		List<BillingAccount> billingAccountList = new LinkedList<BillingAccount>();
		try (Connection connection = ConnectionManager.getConnection()) {
			billingAccountList = billingAccountDao.get(connection);
			connection.commit();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return billingAccountList;
	}

	public List<CustomerBill> getAllCustomerBills() {
		List<CustomerBill> customerBillList = new LinkedList<CustomerBill>();
		try (Connection connection = ConnectionManager.getConnection()) {
			customerBillList = customerBillDao.get(connection);
			connection.commit();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		// TODO Auto-generated method stub
		return customerBillList;
	}

	public List<AppliedCustomerBillingRate> getAllAppliedCustomerBillingRates() {
		List<AppliedCustomerBillingRate> appliedCustomerBillingRateList = new LinkedList<AppliedCustomerBillingRate>();
		try (Connection connection = ConnectionManager.getConnection()) {
			appliedCustomerBillingRateList = appliedCustomerBillingRateDao.get(connection);
			connection.commit();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		// TODO Auto-generated method stub
		return appliedCustomerBillingRateList;
	}

	public List<AppliedBillingTaxRate> getAllAppliedBillingTaxRates() {
		List<AppliedBillingTaxRate> appliedBillingTaxRateList = new LinkedList<AppliedBillingTaxRate>();
		try (Connection connection = ConnectionManager.getConnection()) {
			appliedBillingTaxRateList = appliedBillingTaxRateDao.get(connection);
			connection.commit();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		// TODO Auto-generated method stub
		return appliedBillingTaxRateList;
	}

	public List<Product> getProductByBillingAccountId(Long billingAccountId) {

		List<Product> productList = new LinkedList<Product>();
		try (Connection connection = ConnectionManager.getConnection()) {
			productList = productDao.get(connection, billingAccountId);
			connection.commit();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		// TODO Auto-generated method stub
		return productList;
	}

	public Tariff getTariff(Long tariffId) {
		Tariff tariff = new Tariff();
		try (Connection connection = ConnectionManager.getConnection()) {
			tariff = tariffDao.get(connection, tariffId);
			connection.commit();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		// TODO Auto-generated method stub
		return tariff;
	}

	public void insertCustomerBill(CustomerBill customerBill) {
		try (Connection connection = ConnectionManager.getConnection()) {
			customerBillDao.insertCustomerBill(connection, customerBill);
			connection.commit();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void insertAppliedCustomerBillingRate(AppliedCustomerBillingRate appliedCustomerBillingRate) {
		try (Connection connection = ConnectionManager.getConnection()) {
			appliedCustomerBillingRateDao.insertAppliedCustomerBillingRate(connection, appliedCustomerBillingRate);
			connection.commit();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void insertAppliedBillingTaxRate(AppliedBillingTaxRate appliedBillingTaxRate) {
		try (Connection connection = ConnectionManager.getConnection()) {
			appliedBillingTaxRateDao.insertAppliedBillingTaxRate(connection, appliedBillingTaxRate);
			connection.commit();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public List<CustomerBill> billRun() {
		List<CustomerBill> customerBillList = new ArrayList<CustomerBill>();
		try (Connection connection = ConnectionManager.getConnection()) {

			List<BillingAccount> billingAccountList = billingAccountDao.get(connection);
			List<AppliedCustomerBillingRate> appliedCustomerBillingRateList = new ArrayList<AppliedCustomerBillingRate>();
			List<AppliedBillingTaxRate> appliedBillingTaxRateList = new ArrayList<AppliedBillingTaxRate>();

			for (BillingAccount billingAccount : billingAccountList) {
				// product list
				List<Product> productList = productDao.get(connection, billingAccount.getId());

				// Create a bill for each billing account
				CustomerBill customerBill = new CustomerBill();
				Long billId = customerBillDao.getId(connection);

				// Dates
				Timestamp billDate = new Timestamp(System.currentTimeMillis());
				// 10 days after billing date
				Timestamp dueDate = new Timestamp(System.currentTimeMillis() + 1000 * 60 * 60 * 24 * 10);
				// Fatrualama gunun bir ay sonrasi
				Timestamp nextBillDate = new Timestamp(System.currentTimeMillis() + 1000 * 60 * 60 * 24 * 30);

				customerBill.setId(billId);
				customerBill.setBillDate(billDate);
				customerBill.setLastUpdate(billDate);
				customerBill.setNextBillDate(nextBillDate);
				customerBill.setBillNo(billId + 100000000);
				customerBill.setCategory("monthlyBill");
				customerBill.setRunType("onDemand");
				customerBill.setCurrencyCode("TRY");
				customerBill.setPaymentDueDate(dueDate);
				customerBill.setBillingAccountId(billingAccount.getId());
				customerBill.setAmountDue(0.0);
				customerBill.setTaxExcludedAmount(0.0);
				customerBill.setTaxIncludedAmount(0.0);
				customerBill.setRemainingAmount(0.0);

				// Charge each Product
				for (Product product : productList) {

					// Get tariff info
					Tariff tariff = tariffDao.get(connection, product.getTariffId());

					// Create Applied billing rate items
					Long billRateId = appliedCustomerBillingRateDao.getId(connection);

					AppliedCustomerBillingRate appliedCustomerBillingRate = new AppliedCustomerBillingRate();

					// Set rate values
					appliedCustomerBillingRate.setId(billRateId);
					appliedCustomerBillingRate.setBillId(billId);
					appliedCustomerBillingRate.setName(product.getName());
					appliedCustomerBillingRate.setDescription(product.getName());
					appliedCustomerBillingRate.setIsBilled("Y");
					appliedCustomerBillingRate.setChargeDate(billDate);
					appliedCustomerBillingRate.setRateType("USAGE");
					appliedCustomerBillingRate.setBaseRateType("USAGE");
					appliedCustomerBillingRate.setChargeType("RECURRING");
					appliedCustomerBillingRate.setRecordType(null);
					appliedCustomerBillingRate.setTaxExcludedAmount(tariff.getPrice());
					appliedCustomerBillingRate.setQuantity(1.0);
					appliedCustomerBillingRate.setTaxJurisdiction("TR");
					appliedCustomerBillingRate.setTaxCategory("KDV 18 ");
					appliedCustomerBillingRate.setGeneralLedgerCode(null);
					appliedCustomerBillingRate.setBillingAccountId(billingAccount.getId());
					appliedCustomerBillingRate.setProductId(product.getId());

					// Create billing tax rates
					AppliedBillingTaxRate appliedBillingTaxRate = new AppliedBillingTaxRate();
					appliedBillingTaxRate.setTaxCategory("KDV 18");
					appliedBillingTaxRate.setTaxJurisdiction("TR");
					appliedBillingTaxRate.setTaxType("KDV 18");
					appliedBillingTaxRate.setTaxRate(18.0);
					appliedBillingTaxRate.setTaxBaseAmount(appliedCustomerBillingRate.getTaxExcludedAmount());
					appliedBillingTaxRate
							.setTaxAmount(Math.round(appliedCustomerBillingRate.getTaxExcludedAmount() * 18.0) / 100.0);
					appliedBillingTaxRate.setCurrencyCode("TRY");
					appliedBillingTaxRate.setGeneralLedgerCode(null);
					appliedBillingTaxRate.setBillId(billId);
					appliedBillingTaxRate.setBillRateId(billRateId);

					appliedCustomerBillingRate.setTaxIncludedAmount(
							appliedBillingTaxRate.getTaxBaseAmount() + appliedBillingTaxRate.getTaxAmount());

					customerBill.setAmountDue(
							customerBill.getAmountDue() + appliedCustomerBillingRate.getTaxIncludedAmount());
					customerBill.setRemainingAmount(customerBill.getAmountDue());
					customerBill.setTaxExcludedAmount(
							customerBill.getTaxExcludedAmount() + appliedCustomerBillingRate.getTaxExcludedAmount());
					customerBill.setTaxIncludedAmount(
							customerBill.getTaxIncludedAmount() + appliedCustomerBillingRate.getTaxIncludedAmount());

					appliedBillingTaxRateList.add(appliedBillingTaxRate);
					appliedCustomerBillingRateList.add(appliedCustomerBillingRate);
				}
				customerBillList.add(customerBill);

			}

			for (CustomerBill customerBill : customerBillList) {
				customerBillDao.insertCustomerBill(connection, customerBill);
			}
			for (AppliedCustomerBillingRate appliedCustomerBillingRate : appliedCustomerBillingRateList) {
				appliedCustomerBillingRateDao.insertAppliedCustomerBillingRate(connection, appliedCustomerBillingRate);
			}
			for (AppliedBillingTaxRate appliedBillingTaxRate : appliedBillingTaxRateList) {
				appliedBillingTaxRateDao.insertAppliedBillingTaxRate(connection, appliedBillingTaxRate);
			}

			connection.commit();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return customerBillList;
	}

}
