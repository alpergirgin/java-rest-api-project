package com.i2i.staj2020.billing.engine.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.LinkedList;
import java.util.List;

import com.i2i.staj2020.billing.engine.cache.SQLCache;
import com.i2i.staj2020.billing.model.bill.AppliedCustomerBillingRate;

/**
 * Billing Data Access Object
 * 
 * @author Nida Cikaray
 *
 */
public class AppliedCustomerBillingRateDao {
	public List<AppliedCustomerBillingRate> get(Connection connection) {
		List<AppliedCustomerBillingRate> appliedCustomerBillingRateList = new LinkedList<AppliedCustomerBillingRate>();
		String query = SQLCache.SELECT.ALL_APPLIED_CUSTOMER_BILLING_RATE;
		try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
			try (ResultSet resultSet = preparedStatement.executeQuery()) {
				while (resultSet.next()) {
					AppliedCustomerBillingRate appliedCustomerBillingRate = new AppliedCustomerBillingRate();
					appliedCustomerBillingRate.setId(resultSet.getLong("ID"));
					appliedCustomerBillingRate.setBillId(resultSet.getLong("BILL_ID"));
					appliedCustomerBillingRate.setName(resultSet.getString("NAME"));
					appliedCustomerBillingRate.setDescription(resultSet.getString("DESCRIPTION"));
					appliedCustomerBillingRate.setIsBilled(resultSet.getString("IS_BILLED"));
					appliedCustomerBillingRate.setChargeDate(resultSet.getTimestamp("CHARGE_DATE"));
					appliedCustomerBillingRate.setRateType(resultSet.getString("RATE_TYPE"));
					appliedCustomerBillingRate.setBaseRateType(resultSet.getString("BASE_RATE_TYPE"));
					appliedCustomerBillingRate.setChargeType(resultSet.getString("CHARGE_TYPE"));
					appliedCustomerBillingRate.setRecordType(resultSet.getString("RECORD_TYPE"));
					appliedCustomerBillingRate.setTaxExcludedAmount(resultSet.getDouble("TAX_EXCLUDED_AMOUNT"));
					appliedCustomerBillingRate.setTaxIncludedAmount(resultSet.getDouble("TAX_INCLUDED_AMOUNT"));
					appliedCustomerBillingRate.setQuantity(resultSet.getDouble("QUANTITY"));
					appliedCustomerBillingRate.setTaxJurisdiction(resultSet.getString("TAX_JURISDICTION"));
					appliedCustomerBillingRate.setTaxCategory(resultSet.getString("TAX_CATEGORY"));
					appliedCustomerBillingRate.setGeneralLedgerCode(resultSet.getString("GENERAL_LEDGER_CODE"));
					appliedCustomerBillingRate.setBillingAccountId(resultSet.getLong("BILLING_ACCOUNT_ID"));
					appliedCustomerBillingRate.setProductId(resultSet.getLong("PRODUCT_ID"));
					appliedCustomerBillingRateList.add(appliedCustomerBillingRate);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return appliedCustomerBillingRateList;
	}
	
	public Long getId(Connection connection) {
		
		String query = SQLCache.SEQUENCE.APPLIED_CUSTOMER_BILLING_RATE;
		try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
			try (ResultSet resultSet = preparedStatement.executeQuery()) {
				while (resultSet.next()) {
				
				return resultSet.getLong("SEQ");
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public void insertAppliedCustomerBillingRate(Connection connection, AppliedCustomerBillingRate appliedCustomerBillingRate) {

		try (PreparedStatement preparedStatement = connection.prepareStatement(SQLCache.INSERT.APPLIED_CUSTOMER_BILLING_RATE)) {

			preparedStatement.setLong(1, appliedCustomerBillingRate.getId());
			preparedStatement.setLong(2, appliedCustomerBillingRate.getBillId());
			preparedStatement.setString(3, appliedCustomerBillingRate.getName());
			preparedStatement.setString(4, appliedCustomerBillingRate.getDescription());
			preparedStatement.setString(5, appliedCustomerBillingRate.getIsBilled());
			preparedStatement.setTimestamp(6, appliedCustomerBillingRate.getChargeDate());
			preparedStatement.setString(7, appliedCustomerBillingRate.getRateType());
			preparedStatement.setString(8, appliedCustomerBillingRate.getBaseRateType());
			preparedStatement.setString(9, appliedCustomerBillingRate.getChargeType());
			preparedStatement.setString(10, appliedCustomerBillingRate.getRecordType());
			preparedStatement.setDouble(11, appliedCustomerBillingRate.getTaxExcludedAmount());
			preparedStatement.setDouble(12, appliedCustomerBillingRate.getTaxIncludedAmount());
			preparedStatement.setDouble(13, appliedCustomerBillingRate.getQuantity());
			preparedStatement.setString(14, appliedCustomerBillingRate.getTaxJurisdiction());
			preparedStatement.setString(15, appliedCustomerBillingRate.getTaxCategory());
			preparedStatement.setString(16, appliedCustomerBillingRate.getGeneralLedgerCode());
			preparedStatement.setLong(17, appliedCustomerBillingRate.getBillingAccountId());
			preparedStatement.setLong(18, appliedCustomerBillingRate.getProductId());
			preparedStatement.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void update(Connection connection, AppliedCustomerBillingRate appliedCustomerBillingRate) {
		try (PreparedStatement preparedStatement = connection
				.prepareStatement(SQLCache.UPDATE.APPLIED_CUSTOMER_BILLING_RATE_LAST_BILL_DATE)) {
			// �rnek: preparedStatement.setLong(1, billingAccount.getId());
			Timestamp lastBillDate = new Timestamp(System.currentTimeMillis());
			preparedStatement.setTimestamp(1, lastBillDate);
			preparedStatement.setLong(2, appliedCustomerBillingRate.getId());
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	

	
}
