package com.i2i.staj2020.billing.engine.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.LinkedList;
import java.util.List;

import com.i2i.staj2020.billing.engine.cache.SQLCache;
import com.i2i.staj2020.billing.model.Product;
import com.i2i.staj2020.billing.model.Tariff;
import com.i2i.staj2020.billing.model.bill.CustomerBill;

public class ProductDao {

	public List<Product> get(Connection connection, Long billingAccountId) {

		List<Product> productList = new LinkedList<Product>();
		String query = SQLCache.SELECT.PRODUCT;
		try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
			preparedStatement.setLong(1, billingAccountId);
			try (ResultSet resultSet = preparedStatement.executeQuery()) {
				while (resultSet.next()) {
					Product product = new Product();
					product.setId(resultSet.getLong("ID"));
					product.setName(resultSet.getString("NAME"));
					product.setParentId(resultSet.getLong("PARENT_ID"));
					product.setTariffId(resultSet.getLong("TARIFF_ID"));
					product.setType(resultSet.getInt("TYPE"));
					product.setBillingAccountId(resultSet.getLong("BILLING_ACCOUNT_ID"));

					productList.add(product);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return productList;
	}

//	public Tariff get(Connection connection, Long id) {
//
//		String query = SQLCache.SELECT.TARIFF;
//		try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
//			preparedStatement.setLong(1, id);
//			try (ResultSet resultSet = preparedStatement.executeQuery()) {
//				if (resultSet.next()) {
//					Tariff tariff = new Tariff();
//					tariff.setId(resultSet.getLong("ID"));
//					tariff.setPrice(resultSet.getDouble("PRICE"));
//					tariff.setName(resultSet.getString("NAME"));
//
//					return tariff;
//				}
//			}
//		} catch (SQLException e) {
//			e.printStackTrace();
//		}
//		return null;
//	}

	public void insertProduct(Connection connection, Product product) {

		try (PreparedStatement preparedStatement = connection.prepareStatement(SQLCache.INSERT.PRODUCT)) {

			preparedStatement.setString(1, product.getName());
			preparedStatement.setLong(2, product.getParentId());
			preparedStatement.setLong(3, product.getTariffId());
			preparedStatement.setInt(4, product.getType());
			preparedStatement.setLong(5, product.getBillingAccountId());
			preparedStatement.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void update(Connection connection, Product product) {
		try (PreparedStatement preparedStatement = connection
				.prepareStatement(SQLCache.UPDATE.PRODUCT_LAST_BILL_DATE)) {
			// �rnek: preparedStatement.setLong(1, billingAccount.getId());
			Timestamp lastBillDate = new Timestamp(System.currentTimeMillis());
			preparedStatement.setTimestamp(1, lastBillDate);
			preparedStatement.setLong(2, product.getId());
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}
}
