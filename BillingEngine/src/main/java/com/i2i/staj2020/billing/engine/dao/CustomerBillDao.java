package com.i2i.staj2020.billing.engine.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.LinkedList;
import java.util.List;

import com.i2i.staj2020.billing.engine.cache.SQLCache;
import com.i2i.staj2020.billing.manager.ConnectionManager;
import com.i2i.staj2020.billing.model.bill.CustomerBill;

/**
 * Billing Data Access Object
 * 
 * @author Nida Cikaray
 *
 */
public class CustomerBillDao {

	public List<CustomerBill> get(Connection connection) {
		List<CustomerBill> customerBillList = new LinkedList<CustomerBill>();
		String query = SQLCache.SELECT.ALL_CUSTOMER_BILL;
		try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
			try (ResultSet resultSet = preparedStatement.executeQuery()) {
				while (resultSet.next()) {
					CustomerBill customerBill = new CustomerBill();
					customerBill.setId(resultSet.getLong("ID"));
					customerBill.setBillNo(resultSet.getLong("BILL_NO"));
					customerBill.setCategory(resultSet.getString("CATEGORY"));
					customerBill.setRunType(resultSet.getString("RUN_TYPE"));
					customerBill.setCurrencyCode(resultSet.getString("CURRENCY_CODE"));
					customerBill.setPaymentDueDate(resultSet.getTimestamp("PAYMENT_DUE_DATE"));
					customerBill.setAmountDue(resultSet.getDouble("AMOUNT_DUE"));
					customerBill.setRemainingAmount(resultSet.getDouble("REMAINING_AMOUNT"));
					customerBill.setTaxExcludedAmount(resultSet.getDouble("TAX_EXCLUDED_AMOUNT"));
					customerBill.setTaxIncludedAmount(resultSet.getDouble("TAX_INCLUDED_AMOUNT"));
					customerBill.setBillingAccountId(resultSet.getLong("BILLING_ACCOUNT_ID"));
					customerBill.setBillDate(resultSet.getTimestamp("BILL_DATE"));
					customerBill.setLastUpdate(resultSet.getTimestamp("LAST_UPDATE"));
					customerBill.setNextBillDate(resultSet.getTimestamp("NEXT_BILL_DATE"));
					customerBillList.add(customerBill);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return customerBillList;
	}

	public void insertCustomerBill(Connection connection, CustomerBill customerBill) {

		try (PreparedStatement preparedStatement = connection.prepareStatement(SQLCache.INSERT.CUSTOMER_BILL)) {

			preparedStatement.setLong(1, customerBill.getId());
			preparedStatement.setLong(2, customerBill.getBillNo());
			preparedStatement.setString(3, customerBill.getCategory());
			preparedStatement.setString(4, customerBill.getRunType());
			preparedStatement.setString(5, customerBill.getCurrencyCode());
			preparedStatement.setTimestamp(6, customerBill.getPaymentDueDate());
			preparedStatement.setDouble(7, customerBill.getAmountDue());
			preparedStatement.setDouble(8, customerBill.getRemainingAmount());
			preparedStatement.setDouble(9, customerBill.getTaxExcludedAmount());
			preparedStatement.setDouble(10, customerBill.getTaxIncludedAmount());
			preparedStatement.setLong(11, customerBill.getBillingAccountId());
			preparedStatement.setTimestamp(12, customerBill.getBillDate());
			preparedStatement.setTimestamp(13, customerBill.getLastUpdate());
			preparedStatement.setTimestamp(14, customerBill.getNextBillDate());
			preparedStatement.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void update(Connection connection, CustomerBill customerBill) {
		try (PreparedStatement preparedStatement = connection
				.prepareStatement(SQLCache.UPDATE.CUSTOMER_BILL_LAST_BILL_DATE)) {
			// �rnek: preparedStatement.setLong(1, billingAccount.getId());
			Timestamp lastBillDate = new Timestamp(System.currentTimeMillis());
			preparedStatement.setTimestamp(1, lastBillDate);
			preparedStatement.setLong(2, customerBill.getId());
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}
	
	public Long getId(Connection connection) {
		
		String query = SQLCache.SEQUENCE.CUSTOMER_BILL;
		try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
			try (ResultSet resultSet = preparedStatement.executeQuery()) {
				while (resultSet.next()) {
				
				return resultSet.getLong("SEQ");
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

}
