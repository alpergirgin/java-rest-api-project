package com.i2i.staj2020.billing.engine.test;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.i2i.staj2020.billing.engine.service.BillingEngineService;
import com.i2i.staj2020.billing.model.bill.AppliedBillingTaxRate;


public class TestAppliedBillingTaxRate {

	BillingEngineService billingEngineService = new BillingEngineService();

	@Test
	public void test_get() {
		List<AppliedBillingTaxRate> appliedBillingTaxRateList = billingEngineService.getAllAppliedBillingTaxRates();
		for (int i = 0; i < appliedBillingTaxRateList.size(); i++) {
			AppliedBillingTaxRate appliedBillingTaxRate = appliedBillingTaxRateList.get(i);
			System.out.println(toJson(appliedBillingTaxRate));
		}

	}

	@Test
	public void test_insert() {
		try {
			AppliedBillingTaxRate appliedBillingTaxRate = fromJson();

			billingEngineService.insertAppliedBillingTaxRate(appliedBillingTaxRate);

			System.out.println("insert basarili!");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private String toJson(AppliedBillingTaxRate appliedBillingTaxRate) {
		try {
			JsonMapper mapper = new JsonMapper();
			mapper.enable(SerializationFeature.INDENT_OUTPUT);
			return mapper.writeValueAsString(appliedBillingTaxRate);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return null;

	}
	
	private AppliedBillingTaxRate fromJson() {
		try {
			JsonMapper mapper = new JsonMapper();
			
			return mapper.readValue("{" + 
					"  \"id\" : 12345897," + 
					"  \"taxCategory\" : 12," + 
					"  \"taxJurisdiction\" : \"monthly\"," + 
					"  \"taxType\" : \"onCycle\"," + 
					"  \"taxRate\" : 12.0," + 
					"  \"taxBaseAmount\" : 1.0," + 
					"  \"taxAmount\" : 10.0," + 
					"  \"currencyCode\" : \"fnhngh\"," + 
					"  \"generalLedgerCode\" : \"aaaa\"," + 
					"  \"billId\" : 5," + 
					"  \"billRateId\" : 0" + 
					"}", AppliedBillingTaxRate.class);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}
