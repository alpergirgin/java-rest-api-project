package com.i2i.staj2020.billing.engine.test;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.junit.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.i2i.staj2020.billing.engine.service.BillingEngineService;
import com.i2i.staj2020.billing.model.bill.CustomerBill;

public class TestCustomerBill {
	
	BillingEngineService billingEngineService = new BillingEngineService();
	 
	@Test
	public void test_get() {
		List<CustomerBill> customerBillList = billingEngineService.getAllCustomerBills();
		for (int i = 0; i < customerBillList.size(); i++) {
			CustomerBill customerBill = customerBillList.get(i);
			System.out.println(toJson(customerBill));
		}

	} 
	
	@Test
	public void test_insert() {
		try {
		CustomerBill customerBill= fromJson();
		
		billingEngineService.insertCustomerBill(customerBill);
		
		System.out.println("insert basarili!");
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	@Test
	public void test_billRun() {
		List<CustomerBill> customerBillList = billingEngineService.billRun();
		
		for (CustomerBill customerBill : customerBillList) {
			System.out.println(toJson(customerBill));
		}
	}
	
	

	private String toJson(CustomerBill customerBill) {
		try {
			JsonMapper mapper = new JsonMapper();
			mapper.enable(SerializationFeature.INDENT_OUTPUT);
			return mapper.writeValueAsString(customerBill);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return null;

	}
	
	private CustomerBill fromJson() {
		try {
			JsonMapper mapper = new JsonMapper();
			
			return mapper.readValue("{" + 
					"  \"id\" : 123457," + 
					"  \"billNo\" : 12," + 
					"  \"category\" : \"monthly\"," + 
					"  \"runType\" : \"onCycle\"," + 
					"  \"currencyCode\" : \"USD\"," + 
					"  \"paymentDueDate\" : 1293994049000," + 
					"  \"amountDue\" : 10.0," + 
					"  \"remainingAmount\" : 99.5," + 
					"  \"taxExcludedAmount\" : 8.6," + 
					"  \"taxIncludedAmount\" : 7.5," + 
					"  \"billingAccountId\" : 0," + 
					"  \"billDate\" : 1293994049000," + 
					"  \"lastUpdate\" : 1293994049000," + 
					"  \"nextBillDate\" : 1293994049000," + 
					"  \"taxItem\" : null" + 
					"}", CustomerBill.class);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}
