package com.i2i.staj2020.billing.engine.test;

import java.util.List;

import org.junit.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.i2i.staj2020.billing.engine.service.BillingEngineService;
import com.i2i.staj2020.billing.model.BillingAccount;

public class TestBillingAccount {

	BillingEngineService billingEngineService = new BillingEngineService();

	@Test
	public void test_get() {
		List<BillingAccount> billingAccountList = billingEngineService.getAllBillingAccounts();

		for (int i = 0; i < billingAccountList.size(); i++) {
			BillingAccount billingAccount = billingAccountList.get(i);
			System.out.println(toJson(billingAccount));
		}
	}

	private String toJson(BillingAccount billingAccount) {
		try {
			JsonMapper mapper = new JsonMapper();
			mapper.enable(SerializationFeature.INDENT_OUTPUT);
			return mapper.writeValueAsString(billingAccount);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return null;
	}
}
