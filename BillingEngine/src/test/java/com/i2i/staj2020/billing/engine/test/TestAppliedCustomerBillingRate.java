package com.i2i.staj2020.billing.engine.test;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.i2i.staj2020.billing.engine.service.BillingEngineService;
import com.i2i.staj2020.billing.model.bill.AppliedCustomerBillingRate;


public class TestAppliedCustomerBillingRate {

	BillingEngineService billingEngineService = new BillingEngineService();
	
	
	@Test
	public void test_get() {
		List<AppliedCustomerBillingRate> appliedCustomerBillingRateList = billingEngineService.getAllAppliedCustomerBillingRates();
		for (int i = 0; i < appliedCustomerBillingRateList.size(); i++) {
			AppliedCustomerBillingRate appliedCustomerBillingRate = appliedCustomerBillingRateList.get(i);
			System.out.println(toJson(appliedCustomerBillingRate));
		}

	}
	
	@Test
	public void test_insert() {
		try {
			AppliedCustomerBillingRate appliedCustomerBillingRate= fromJson();
		
		billingEngineService.insertAppliedCustomerBillingRate(appliedCustomerBillingRate);
		
		System.out.println("insert basarili!");
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private String toJson(AppliedCustomerBillingRate appliedCustomerBillingRate) {
		try {
			JsonMapper mapper = new JsonMapper();
			mapper.enable(SerializationFeature.INDENT_OUTPUT);
			return mapper.writeValueAsString(appliedCustomerBillingRate);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return null;

	}
	
	private AppliedCustomerBillingRate fromJson() {
		try {
			JsonMapper mapper = new JsonMapper();
			
			return mapper.readValue("{" + 
					"  \"id\" : 1234578," + 
					"  \"billId\" : 6," + 
					"  \"name\" : \"monthly\"," + 
					"  \"description\" : \"orfycl\"," + 
					"  \"isBilled\" : \"djfjf\"," + 
					"  \"chargeDate\" : 1293994049000," + 
					"  \"rateType\" : \"asfdg\"," + 
					"  \"baseRateType\" : \"uk�kjg\"," + 
					"  \"chargeType\" : \"egghn\"," + 
					"  \"recordType\" : \"jhjn\"," + 
					"  \"taxExcludedAmount\" : 0," + 
					"  \"taxIncludedAmount\" : 9," + 
					"  \"quantity\" : 1," + 
					"  \"taxJurisdiction\" : \"hjnn\"," + 
					"  \"taxCategory\" : \"efrg\"," +  
					"  \"generalLedgerCode\" : \"mfrg\","+
					"  \"billingAccountId\" : 0,"+
					"  \"productId\" : 0"+
					"}", AppliedCustomerBillingRate.class);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	}


